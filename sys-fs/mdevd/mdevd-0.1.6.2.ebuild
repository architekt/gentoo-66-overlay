# Copyright 2019-2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs

DESCRIPTION="Kernel event manager compatible with mdev.conf"
HOMEPAGE="https://www.skarnet.org/software/mdevd/"
SRC_URI="https://www.skarnet.org/software/${PN}/${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~x86"
IUSE="doc"

RDEPEND="
    dev-libs/skalibs:=
    dev-lang/execline:="
DEPEND="${RDEPEND}"

pkg_setup() {
    use doc && HTML_DOCS=( doc/. )
}

src_prepare() {
    default

    ## Avoid QA warning for LDFLAGS addition
    sed -i -e 's/.*-Wl,--hash-style=both$/:/' configure || die

    sed -i -e '/AR := /d' -e '/RANLIB := /d' Makefile || die
}

src_configure() {
    tc-export AR CC RANLIB

    local myconf=(
        --bindir=/bin
        --dynlibdir=/usr/$(get_libdir)
        --libdir=/usr/$(get_libdir)/${PN}
        --with-dynlib=/usr/$(get_libdir)
        --with-lib=/usr/$(get_libdir)/skalibs
        --with-lib=/usr/$(get_libdir)/execline
        --with-sysdeps=/usr/$(get_libdir)/skalibs/sysdeps
        --enable-shared
        --disable-allstatic
        --disable-static-libc
    )

    econf "${myconf[@]}"
}
