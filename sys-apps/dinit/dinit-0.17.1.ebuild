# Copyright 2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs

DESCRIPTION="service manager and init system"
HOMEPAGE="https://davmac.org/projects/dinit/"
SRC_URI="https://github.com/davmac314/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~x86"
IUSE="dbus elogind lto sysinit udev"

RDEPEND="
   dbus? ( sys-apps/dbus )
   elogind? ( sys-auth/elogind )
   udev? ( virtual/udev )"

src_prepare() {
   eapply_user
   sed -i -e "s|CXX=\$compiler|CXX=$(tc-getCXX)|" configs/mconfig.Linux.sh || die
   #sed -i -e "s|LDFLAGS=|LDFLAGS=${LDFLAGS}|" configs/mconfig.Linux.sh || die
   #sed -i -e 's|Os|O2|' configs/mconfig.Linux.sh || die
}

src_configure() {
   if use !sysinit; then
      sed -i -e 's|BUILD_SHUTDOWN=yes|BUILD_SHUTDOWN=no|' configs/mconfig.Linux.sh || die
   fi

   if use !lto; then
    sed -i -e 's|test_compiler_arg "$compiler" -flto||' configs/mconfig.Linux.sh || die
   fi
}

