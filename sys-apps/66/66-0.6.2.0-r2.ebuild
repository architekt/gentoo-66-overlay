# Copyright 2019-2022 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs

DESCRIPTION="Small tools built around s6 and s6-rc programs"
HOMEPAGE="https://git.obarun.org/Obarun/66/"
SRC_URI="https://git.obarun.org/Obarun/66/-/archive/v${PV}/66-v${PV}.tar.bz2 -> 66-${PV}.tar.bz2"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~x86"
IUSE="doc doc-html man"

RDEPEND="
    dev-libs/skalibs:=
    dev-lang/execline:=
    sys-apps/s6:=
    sys-apps/s6-rc:=
    dev-libs/oblibs:=
    acct-user/s6log
    acct-group/s6log"

DEPEND="${RDEPEND}
    app-text/lowdown"

S="${WORKDIR}/${PN}-v${PV}"

pkg_setup() {
    use doc-html && HTML_DOCS=( doc/html/. )
}

src_prepare() {
    default

    ## Avoid QA warning for LDFLAGS addition
    sed -i -e 's/.*-Wl,--hash-style=both$/:/' configure || die

    sed -i -e '/AR := /d' -e '/RANLIB := /d' Makefile || die

    ## Avoid unexpected path for html documentation, let einstalldocs do the job
    sed -i '/^install:/ s/install-html //' Makefile

    ## 66-boot option for Gentoo
    sed -i 's/66-boot/66-boot -m \/run/' skel/init 
}

src_configure() {
    tc-export AR CC RANLIB

    local myconf=(
        --bindir=/bin
        --dynlibdir=/usr/$(get_libdir)
        --libdir=/usr/$(get_libdir)/${PN}
        --with-dynlib=/usr/$(get_libdir)
        --with-lib=/usr/$(get_libdir)/s6
        --with-lib=/usr/$(get_libdir)/s6-rc
        --with-lib=/usr/$(get_libdir)/oblibs
        --with-lib=/usr/$(get_libdir)/skalibs
        --with-lib=/usr/$(get_libdir)/execline
        --with-s6-log-user=s6log
        --with-s6-log-timestamp=iso
        --with-system-module=/usr/share/66/module
        --with-system-script=/usr/share/66/script
        --with-system-service=/usr/share/66/service
        --with-sysadmin-module=/etc/66/module
        --with-sysadmin-service=/etc/66/service
        --with-sysadmin-service-conf=/etc/66/conf
        --with-sysdeps=/usr/$(get_libdir)/skalibs/sysdeps
        --enable-shared
        --disable-allstatic
        --disable-static-libc
    )

    econf "${myconf[@]}"
}

src_compile() {
    if use doc-html ; then
            pushd "${WORKDIR}/${PN}-v${PV}" > /dev/null || die
                ./doc/make-html.sh || die
            popd > /dev/null   || die
    fi

    if use man ; then
            pushd "${WORKDIR}/${PN}-v${PV}" > /dev/null || die
                ./doc/make-man.sh  || die
            popd > /dev/null   || die
    fi
}

src_install() {
    emake DESTDIR="${D}" install

    use doc-html && einstalldocs

    if ! use doc && use doc-html ; then
        find "${ED}/usr/share/doc" -iname "AUTHOR*" | xargs rm
        find "${ED}/usr/share/doc" -iname "README*" | xargs rm
    fi

    if use doc && ! use doc-html ; then
        dodoc AUTHOR* README*
    fi

    use man && doman doc/man/man*/*.[158]

    mkdir "${D}/sbin"
    mkdir "${D}/usr/bin"

    mv "${D}/etc/66/init"       "${D}/sbin/"
    mv "${D}/etc/66/halt"       "${D}/usr/bin/"
    mv "${D}/etc/66/poweroff"   "${D}/usr/bin/"
    mv "${D}/etc/66/reboot"     "${D}/usr/bin/"
    mv "${D}/etc/66/shutdown"   "${D}/usr/bin/"
}

pkg_postinst() {
    elog ""
    elog "WARNING:"
    elog "If you upgrading to a newer version, execute"
    elog "# 66-enable -t <tree> -F boot-user@<username>"
    elog "Replace <tree> by the name of your tree"
    elog "Replace <username> by the name of your user"
    elog ""
    elog ""
}
