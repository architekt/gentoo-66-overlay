# Copyright 2019-2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 toolchain-funcs

EGIT_REPO_URI="https://git.obarun.org/Obarun/66-tools.git"
EGIT_BRANCH="master"

DESCRIPTION="Small tools and helpers for service scripts execution"
HOMEPAGE=""

LICENSE="ISC"

SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64"
IUSE="doc doc-html man"

RDEPEND="
    dev-libs/skalibs:=
    dev-lang/execline:=
    dev-libs/oblibs:="
DEPEND="${RDEPEND}
    app-text/lowdown"

pkg_setup() {
    use doc-html && HTML_DOCS=( doc/html/. )
}

src_prepare() {
    default

    ## Avoid QA warning for LDFLAGS addition
    sed -i -e 's/.*-Wl,--hash-style=both$/:/' configure || die

    sed -i -e '/AR := /d' -e '/RANLIB := /d' Makefile || die

    ## Avoid unexpected path for html documentation, let einstalldocs do the job
    sed -i '/^install:/ s/install-html //' Makefile
}

src_configure() {
    tc-export AR CC RANLIB

    local myconf=(
        --bindir=/bin
        --dynlibdir=/usr/$(get_libdir)
        --libdir=/usr/$(get_libdir)/${PN}
        --with-dynlib=/usr/$(get_libdir)
        --with-lib=/usr/$(get_libdir)/oblibs
        --with-lib=/usr/$(get_libdir)/skalibs
        --with-lib=/usr/$(get_libdir)/execline
        --with-sysdeps=/usr/$(get_libdir)/skalibs/sysdeps
        --with-ns-rule=/usr/share/66/script/ns
        --enable-shared
        --disable-allstatic
        --disable-static-libc
    )

    econf "${myconf[@]}"
}

src_install() {
    emake DESTDIR="${D}" install install-ns-rule

    use doc-html && einstalldocs

    if ! use doc && use doc-html ; then
        find "${ED}/usr/share/doc" -iname "AUTHOR*" | xargs rm
        find "${ED}/usr/share/doc" -iname "README*" | xargs rm
    fi

    if use doc && ! use doc-html ; then
        dodoc AUTHOR* README*
    fi

    use man && doman doc/man/man*/*.1
}
