
# Copyright 2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson

DESCRIPTION="session/login tracker"
HOMEPAGE="https://github.com/chimera-linux/turnstile"

if [[ ${PV} == *9999 ]]; then
   inherit git-r3
   EGIT_REPO_URI="https://github.com/chimera-linux/turnstile.git"
   EGIT_BRANCH="master"
else
   SRC_URI=""
   KEYWORDS="~amd64"
fi

LICENSE="BSD-2"
SLOT="0"
IUSE="dinit doc"

RDEPEND="
   doc? ( app-text/scdoc )
   sys-libs/pam"

src_configure() {
   local emesonargs=(
      $(meson_feature dinit)
      $(meson_use doc man)
   )

   meson_src_configure
}
