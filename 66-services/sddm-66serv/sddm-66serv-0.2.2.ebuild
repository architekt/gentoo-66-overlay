# Copyright 2019-2022 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="sddm service for 66 tools"
HOMEPAGE="https://framagit.org/architekt/66tools-overlay"
LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~x86"
MY_SERVICE="sddm"

RDEPEND="
    sys-apps/66
    sys-apps/66-tools
    x11-misc/sddm"

src_unpack() {
    mkdir "${S}" || die
}

src_prepare() {
    eapply_user

    for i in ${MY_SERVICE[@]} ; do
        sed -e "s:@VERSION@:${PV}:" "${FILESDIR}"/${i}.66 > ${i} || die
    done
}

src_install() {
    dodir /usr/share/66/service
    insinto /usr/share/66/service

    for i in ${MY_SERVICE[@]} ; do
        doins ${i} || die
    done
}
