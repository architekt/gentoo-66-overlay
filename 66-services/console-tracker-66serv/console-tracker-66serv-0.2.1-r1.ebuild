# Copyright 2019-2022 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="General service to enable any console tracker"
HOMEPAGE="https://framagit.org/pkg/obmods/console-tracker-66serv"
SRC_URI="https://framagit.org/pkg/obmods/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.gz -> ${PN}-${PV}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~x86"
IUSE="doc"

RDEPEND="
   sys-apps/66
   sys-apps/66-tools
   66-services/dbus-66serv"

S="${WORKDIR}/${PN}-v${PV}"

src_configure() {
   local myconf=(
      --bindir=/bin
      --with-system-module=/usr/share/66/module
      --with-system-service=/usr/share/66/service
   )

   econf "${myconf[@]}"
}

src_compile() {
    emake DESTDIR="${D}"
}

src_install() {
    emake DESTDIR="${D}" install
    use doc && dodoc AUTHOR*
}
