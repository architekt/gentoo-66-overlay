# Copyright 2019-2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="complete and portable set of services to properly boot a machine running on 66"
HOMEPAGE="https://git.obarun.org/obmods/boot-66serv"
SRC_URI="https://git.obarun.org/obmods/boot-66serv/-/archive/v${PV}/${PN}-v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~x86"
IUSE="btrfs doc doc-html dmraid mdraid iptables lvm man nftables zfs"

RDEPEND="
   sys-apps/66
   sys-apps/66-tools
   sys-apps/66-modules
   sys-apps/66-opentmpfiles
   sys-apps/s6-linux-utils
   sys-apps/s6-portable-utils
   >=app-shells/bash-4.4_p23-r1
   >=sys-apps/iproute2-5.1.0
   >=sys-apps/kmod-26-r1
   >=sys-apps/util-linux-2.33.2
   btrfs? ( sys-fs/btrfs-progs )
   dmraid? ( sys-fs/dmraid )
   iptables? ( net-firewall/iptables )
   lvm? ( sys-fs/lvm2 )
   mdraid? ( sys-fs/mdadm )
   nftables? ( net-firewall/nftables )
   zfs? ( sys-fs/zfs )"

S="${WORKDIR}/${PN}-v${PV}"

PATCHES=(
   "${FILESDIR}/replace-s6test-with-test.patch"
)

pkg_setup() {
   use doc-html && HTML_DOCS=( doc/html/. )
}

src_configure() {
   local mydate=$(date +%a)
   local myhost="$mydate$RANDOM"
   local myconf=(
      --bindir=/bin
      --libdir=/usr/$(get_libdir)/66
      --with-system-module=/usr/share/66/module
      --with-system-script=/usr/share/66/script
      --with-system-service=/usr/share/66/service
      --HOSTNAME=$myhost
    )

   econf "${myconf[@]}"
}

src_compile() {
   emake DESTDIR="${D}"
}

src_install() {
   emake DESTDIR="${D}" install

   find "${ED}/usr/share" -type d -name "doc" | xargs rm -r
   find "${ED}/usr/share" -type d -name "man" | xargs rm -r

   use doc && dodoc AUTHOR*
   use man && doman doc/man/man*/*.1

   if use doc-html ; then
      docinto html
      dodoc doc/html/boot@.html
   fi
}
