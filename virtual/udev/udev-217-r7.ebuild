# Copyright 2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="virtual to select between different udev daemon providers"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64"

RDEPEND="
   || (
      >=sys-fs/eudev-3.2.14
      sys-apps/systemd-utils[udev]
      sys-fs/udev
      >=sys-apps/systemd-217
   )"
