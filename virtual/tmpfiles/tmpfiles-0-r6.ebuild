# Copyright 2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="virtual to select between different tmpfiles.d handlers"
SLOT="0"
KEYWORDS="~amd64 ~arm"
IUSE="systemd"

RDEPEND="
   !prefix-guest? (
      || (
         sys-apps/66-opentmpfiles
         systemd? ( sys-apps/systemd )
         !systemd? ( sys-apps/systemd-utils[tmpfiles] )
      )
   )"
