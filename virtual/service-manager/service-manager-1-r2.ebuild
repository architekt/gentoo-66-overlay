# Copyright 2019-2022 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="virtual for various service managers"

SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~x86"

RDEPEND="
   prefix-guest? ( >=sys-apps/baselayout-prefix-2.2 )
   !prefix-guest? (
      || (
         sys-apps/66
         sys-apps/openrc
         kernel_linux? (
            || (
               sys-apps/s6-rc
               sys-apps/systemd
               sys-process/runit
               virtual/daemontools
            )
         )
      )
   )"
