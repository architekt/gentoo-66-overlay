# Copyright 2019-2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 toolchain-funcs

EGIT_REPO_URI="https://git.obarun.org/Obarun/oblibs.git"
EGIT_BRANCH="master"
SLOT="0"

DESCRIPTION="66 C library"
HOMEPAGE=""
LICENSE="ISC"

KEYWORDS="~amd64 ~arm ~arm64"
IUSE="doc"

RDEPEND="
    dev-libs/skalibs:=
    dev-lang/execline:="
DEPEND="${RDEPEND}"

src_prepare() {
    default

    ## Avoid QA warning for LDFLAGS addition
    sed -i -e 's/.*-Wl,--hash-style=both$/:/' configure || die

    sed -i -e '/AR := /d' -e '/RANLIB := /d' Makefile || die
}

src_configure() {
    tc-export AR CC RANLIB

    local myconf=(
        --bindir=/bin
        --dynlibdir=/usr/$(get_libdir)
        --libdir=/usr/$(get_libdir)/${PN}
        --with-dynlib=/usr/$(get_libdir)
        --with-lib=/usr/$(get_libdir)/execline
        --with-lib=/usr/$(get_libdir)/skalibs
        --with-sysdeps=/usr/$(get_libdir)/skalibs/sysdeps
        --enable-shared
        --disable-allstatic
        --disable-static-libc
    )

    econf "${myconf[@]}"
}

src_install() {
    emake DESTDIR="${D}" install
    use doc && dodoc AUTHOR* README*
}
